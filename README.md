# Django Google Places

Provides modelling and ability to retrieve and sync data with Google Places (courtesy of python-google-places).

## Requirements

- python-google-places: https://github.com/slimkrazy/python-google-places
- django-toolkit: https://bitbucket.org/alexhayes/django-toolkit

## Installation

```bash
pip install git+https://bitbucket.org/alexhayes/django-toolkit.git
pip install python-google-places
pip install git+https://alexhayes@bitbucket.org/alexhayes/django-google-places.git
```

Once installed add `django_google_places` to your installed apps in your settings.py and the setting `GOOGLE_PLACES_API_KEY`.

```python
INSTALLED_APPS = (
	'django_google_places',
)

GOOGLE_PLACES_API_KEY = 'xyz...'
```

Then, using south, migrate your database.

```bash
./manage.py migrate django_google_places
```

Or, with Django >= 1.7

```bash
./manage.py makemigrations django_google_places
./manage.py migrate
```

## Usage

Importing a bunch of places...

```python
from django_google_places.models import Place
from django_google_places.api import google_places

# Retrieve a list of results
results = google_places.text_search(query='my query')
if results.has_attributions:
	for p in results.places:
		try:
			place = Place.objects.get(api_id=p.id)
		except Place.DoesNotExist:
			place = Place()
		place.populate_from_api(p)
```

Retrieve an individual result using it's reference.

```python
from django_google_places.models import Place
place = Place()
place.reference = 'CnRrAAAAuOJcPFCxvcSCmjMr0j4M7kovaDaNeU5fwlfLVSiOHEJaogZgggPXFBS9AxwgPV4TPxXh2Kjr2xh0AMhkLKSTBnTW-1rM1ZWwlQEzq78hJFEr6XyZZ628AuOpj5VWKFfYdfzptU0vnEEEiORFxuJCoRIQjeYT0FnG2kxaYNZGFDPOkhoUYvhZ_uGnqcozLpPEWA6jWBuHUgc'
place.populate_from_api()

print place.name
# u'Googleplex'

print place.website
# outputs: http://www.google.com/

print place.api_id
# outputs: 3a936e96ddcb18b4fa8a2974ebc8876c3108fef2

print place.reviews.all()
# outputs: [<Review: 5.00 by Mike Jaime>, <Review: 3.00 by Nowelle Sinclair>, <Review: 5.00 by Aileen Blake>, <Review: 5.00 by Alicia Hundley>, <Review: 3.00 by kevin cooke>]

print place.formatted_address
# outputs: u'1600 Amphitheatre Parkway, Mountain View, CA, United States'

print place.address_components.all()
# outputs: [<AddressComponent: 1600>, <AddressComponent: Amphitheatre Parkway>, <AddressComponent: Mountain View>, <AddressComponent: Santa Clara>, <AddressComponent: CA>, <AddressComponent: US>, <AddressComponent: 94043>]

print place.place_types.all()
# outputs: [<PlaceType: Establishment>]
```

## Version

0.1.0 - Beta

## Todo

- Verify urls, views and templates work...

## Contributions

Please fork and submit pull requests as you see fit - I'm open to suggestions about how the modelling can be improved.

## Thanks

Thanks to roi.com.au

## Author

Alex Hayes <alex@alution.com>
